﻿using System;
using System.Drawing;

namespace RgbConverter
{
    public static class Rgb
    {
        /// <summary>
        /// Gets hexadecimal representation source RGB decimal values.
        /// </summary>
        /// <param name="red">The valid decimal value for RGB is in the range 0-255.</param>
        /// <param name="green">The valid decimal value for RGB is in the range 0-255.</param>
        /// <param name="blue">The valid decimal value for RGB is in the range 0-255.</param>
        /// <returns>Returns hexadecimal representation source RGB decimal values.</returns>
        public static string GetHexRepresentation(int red, int green, int blue)
        {
            var table = new Dictionary<int, string>()
            {
                { 0,  "0" },
                { 1,  "1" },
                { 2,  "2" },
                { 3,  "3" },
                { 4,  "4" },
                { 5,  "5" },
                { 6,  "6" },
                { 7,  "7" },
                { 8,  "8" },
                { 9,  "9" },
                { 10, "A" },
                { 11, "B" },
                { 12, "C" },
                { 13, "D" },
                { 14, "E" },
                { 15, "F" },
            };
            double hr1 = (double)red / 16;
            double hr2 = (hr1 - (int)hr1) * 16;
            double hg1 = (double)green / 16;
            double hg2 = (hg1 - (int)hg1) * 16;
            double hb1 = (double)blue / 16;
            double hb2 = (hb1 - (int)hb1) * 16;
            if (green < 0)
            {
                hg1 = 0;
                hg2 = 0;
            }

            if (blue > 255)
            {
                hb1 = 15;
                hb2 = 15;
            }

            string hex = table[(int)hr1] + table[(int)hr2] + table[(int)hg1] + table[(int)hg2] + table[(int)hb1] + table[(int)hb2];
            return hex;
        }
    }
}
